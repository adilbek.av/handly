# Serve on production

```
php artisan serve
npm run prod
cp public/css/app.css public/css/temp.css
sed -e 's/\?[a-z0-9]*[\)]/\)/g' -e 's/\?[a-z0-9]*[#]/#/g' public/css/temp.css > public/css/app.css
rm public/css/temp.css
```

# preparation of files for Cordova
```
sed -e 's|url(/fonts|url(../fonts|g' public/css/app.css > public/css/cordova-app.css
sed -e 's|src:"/img|src:"img|g' public/js/app.js > public/js/cordova-app.js
```