<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="initial-scale=1, user-scalable=no, width=device-width, height=device-height, viewport-fit=cover">

    <title>{{ setting('site.title') }}</title>

    <link rel="stylesheet" href="{{ url('https://fonts.googleapis.com/css?family=PT+Serif') }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    {{--        <meta http-equiv="Content-Security-Policy" content="default-src 'self' data: gap: https://ssl.gstatic.com 'unsafe-eval'; style-src 'self' 'unsafe-inline'; media-src *; img-src 'self' data: content:;">--}}
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">


    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/manifest.json">
    <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#6aa6ff">
    <link rel="shortcut icon" href="/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#6aa6ff">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml">
    <meta name="theme-color" content="#6aa6ff">
    <meta name="Description" content="Ym-ishara.kz — платформа по изучению языка жестов путем просмотра обучающего видео.">
    <script src="{{ mix('js/onsen.js') }}"></script>
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script>
        if ('serviceWorker' in navigator) {

            let swFile = ''
            if (ons.platform.isAndroid()) {
                swFile = '/sw-android.js'
            } else if (ons.platform.isIOS()) {
                swFile = '/sw-ios.js'
            }

            if (swFile) {
                navigator.serviceWorker.register(swFile)
                    .then(registration => {
                        console.log("Service Worker Registered");

                        // We don't want to check for updates on first load or we will get a false
                        // positive. registration.active will be falsy on first load.
                        if (registration.active) {
                            // Check if an updated sw-android.js was found
                            registration.addEventListener('updatefound', () => {
                                console.log('Update found. Waiting for install to complete.');
                                const installingWorker = registration.installing;

                                // Watch for changes to the worker's state. Once it is "installed", our cache
                                // has been updated with our new files, so we can prompt the user to instantly
                                // reload.
                                installingWorker.addEventListener('statechange', () => {
                                    if (installingWorker.state === 'installed') {
                                        console.log('Install complete. Triggering update prompt.');
                                        onUpdateFound();
                                    }
                                });
                            });
                        }
                    })
                    .catch(e => console.log(e));
            }
        }

        function onUpdateFound() {
            ons.notification.confirm('Новое обновление готово. Хотите обновить сейчас?')
                .then(buttonIndex => {
                    if (buttonIndex === 1) {
                        location.reload();
                    }
                });
        }
    </script>
</head>
<body>
<div id="app">
    <noscript class="my-3 text-center">Does not support JavaScript!</noscript>
</div>
</body>
</html>
