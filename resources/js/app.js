require('./bootstrap');

import Vue from 'vue';
import VueOnsen from 'vue-onsenui';

// Components
import Splitter from './pages/Splitter'

Vue.use(VueOnsen);

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.baseURL = 'https://ym-ishara.kz';

new Vue({
    el: '#app',
    render: h => h(Splitter)
});
