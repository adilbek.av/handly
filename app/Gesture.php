<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;


class Gesture extends Model
{
    use Translatable, SoftDeletes;
    protected $translatable = ['word'];
    protected $fillable = ['word', 'video', 'youtube_url'];
    protected $appends = ['cover'];

    public function tags() {
        return $this->belongsToMany(Tag::class, 'tag_gestures');
    }

    public function getCoverAttribute() {
        if ($this->youtube_url) {
            return 'https://img.youtube.com/vi'.substr($this->youtube_url, strrpos($this->youtube_url, '/')).'/hqdefault.jpg';
        }
        return '';
    }
}
