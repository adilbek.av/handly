<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;


class Tag extends Model
{
    use Translatable, SoftDeletes;
    protected $translatable = ['name'];
    protected $fillable = ['name'];
}
