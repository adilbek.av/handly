<?php

namespace App\Http\Controllers;

use App\Gesture;
use App\Offer;
use App\Tag;

class ApiController extends Controller
{

    public function __construct()
    {
        app()->setLocale('kk');
    }

    public function getTags()
    {
        return response()->json(Tag::orderBy('name')->get());
    }

    public function getGestures()
    {
        $gestures = Gesture::with('tags')->when(request('tag'), function ($query) {
            return $query->whereHas('tags', function ($query) {
                $query->whereTagId(request('tag'));
            });
        })->when(request('letter'), function ($query) {
            return $query->where('word', 'LIKE', request('letter') . "%");
        })->when(request('q'), function ($query) {
            return $query->where('word', 'LIKE', "%" . request('q') . "%");
        })->orderBy('word')->paginate((int)request('per_page'), ['*'], 'page', request('page'));


        return response()->json($gestures);
    }

    public function sendOffer()
    {
        request()->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'text' => 'required'
        ]);

        Offer::create(request()->all());

        return response()->json(['success' => true]);
    }
}
